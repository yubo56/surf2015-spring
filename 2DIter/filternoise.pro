pro filterNoise, numSamples, gaussHeight, numGaussians
; arguments 
;       numSamples - number of times to try this filtering
;       gaussHeight - height of Gaussians being inserted (width/number are fixed in program)
;       numGaussians - number of Gaussians to insert and seek
; return



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 256. ; size of grid
power = 1; 8.0 / 3
const = 0.01
; constants of the white noise

;pt source params
gaussPixWidth = 5; width in number of pixels


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; useful gaussian
gaussian = GAUSSIAN_FUNCTION ( [( gaussPixWidth - 1 ) / 2, ( gaussPixWidth - 1 ) / 2])
    ; IDL generates Gaussians of FWHM (width-1)/2
gaussWidth = sqrt(n_elements(gaussian))

; make specdens
kx = findgen(range) - range/2
ky = kx
k = sqrt((kx ## replicate(1L,range))^2 + rotate((ky ## replicate(1L,range))^2, 1))
k[range / 2 - 1, range / 2] = 1
specDens = 0.1 / k ^ power + const

; store cumulative amplitudes
ampsCum = []

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GENERATE NOISE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; make noise in k space, random every time
noise = genNoise(specDens)

; add Gaussian
retVal = addfunc(noise, gaussHeight * gaussian, numGaussians)
signal = retVal.sig
xcoords = retVal.xs
ycoords = retVal.ys

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;OPTIMAL FILTERING;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

orig = signal
; put first one in
retVal = subtractmax(signal, gaussian, specDens)
loc = retVal.loc
amp = retVal.amp
signal = retVal.sig
foundX = [loc[0]]
foundY = [loc[1]]
amps = [amp]

; subtract until next highest peak is repeated, then break
repeat begin
    retVal = subtractmax(signal, gaussian, specDens)
    loc = retVal.loc
    amp = retVal.amp
    signal = retVal.sig

    ; add to foundX, foundY iff not already in, else break
    ind = where(abs(foundY - loc[1]) le 2)

    if (ind ne -1) and (where(abs(foundY - loc[1]) le 2) eq ind) then begin
        ; found already, so break
        break
    endif else begin; not already found, so add
        foundX = [foundX, loc[0]]
        foundY = [foundY, loc[1]]
        amps = [amps, amp]
    endelse
endrep until amp eq -1
stop

; remove the extra -1 at the end
if amps[N_ELEMENTS(AMPS) - 1] eq -1 then begin
    amps = amps[0:N_ELEMENTS(amps) - 2]
    foundX = foundX[0:N_ELEMENTS(foundX) - 2]
    foundY = foundY[0:N_ELEMENTS(foundY) - 2]
endif

; pretty print results
print, 'Cols: X, foundX, Y, foundY, (foundX, foundY, amps) sorted by amp'
print, transpose([[xcoords[sort(xcoords)]], [foundX[sort(foundX)]], $
    [ycoords[sort(ycoords)]], [foundY[sort(foundY)]], $
    [foundX[reverse(sort(amps))]], [foundY[reverse(sort(amps))]],$
    [amps[reverse(sort(amps))]]])
    ; print peaks in order found, then separately positions/amplitudes

print, 'Min dist btw peaks, gaussSigma, width of gaussian Kernel'
print, [min(distance_measure(transpose([[xcoords[uniq(xcoords)]],[ycoords[uniq(ycoords)]]]))), $
    gaussPixWidth, gaussWidth]
    ; prints min distance between peaks, sigma of gaussian, width of gauss signal

print, 'Noise level'
print, retVal.sigm

stop

; store to ampsCum
ampsCum = [amps]

;; update estimates sequentially
for cycles=0, numSamples do begin
    ; add in signals and resubtract
    for i=0, N_ELEMENTS(amps) - 1 do begin; index to be recalculated
        signal[xcoords[i], ycoords[i]] += amps[i] * gaussian; add in gaussian
        retVal = subtractmax(signal, gaussian, specDens)
        loc = retVal.loc
        xcoords[i] = loc[0]
        ycoords[i] = loc[1]
        amps[i] = retVal.amp
        signal = retVal.sig
    endfor

    ; add to ampsCum
    ampsCum = [[ampsCum], [amps]]
endfor

sizeCum = size(ampsCum)
plot, ampsCum[0, 0: sizeCum[2] - 1], YRANGE=[min(ampsCum), max(ampsCum)]
for i=0, sizeCum[1] - 1 do begin
    oplot, ampsCum[i, 0: sizeCum[2] - 1]
endfor
END
