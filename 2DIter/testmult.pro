pro testmult, gaussHeight, numGaussians, numIts
; arguments 
;       gaussHeight - height of Gaussians being inserted (width/number are fixed in program)
;       numGaussians - number of Gaussians to insert and seek
; return



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 1024. ; range, the full range will go from [0, N] U [-N, -1]
power = 1
const = 0.01
; constants of the white noise

;pt source params
gaussPixWidth = 5; width in number of pixels


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; useful gaussian
gaussian = GAUSSIAN_FUNCTION ( [( gaussPixWidth - 1 ) / 2, ( gaussPixWidth - 1 ) / 2])
    ; IDL generates Gaussians of FWHM (width-1)/2
gaussWidth = sqrt(n_elements(gaussian))

; make specdens
kx = findgen(range) - range/2
ky = kx
k = sqrt((kx ## replicate(1L,range))^2 + rotate((ky ## replicate(1L,range))^2, 1))
k[range / 2 - 1, range / 2] = 1
specDens = 0.1 / k ^ power + const

moments=dblarr(4)

for j=1, numIts do begin
    ; get a multiple added-Gaussian signal
    noise = genNoise(specDens)
    retVal = addfunc(noise, gaussHeight * gaussian, numGaussians)
    signal = retVal.sig
    

    ; amplitudes
    amps = []

    ; subtract just numGaussians pulses
    for i=1, numGaussians do begin
        sig2 = subtractmaxfs(signal, gaussian, specDens)
        signal = sig2.sig
        amps = [amps, sig2.amp]
    endfor

    ; moments += moment(amps)
    moments = [[moments], [moment(amps)]]
endfor

; print, moments / numIts
stop

end
