function subtractmaxof, signal, kernel, specDens
; Arguments
;       signal - real signal from which to subtract kernel once, at max convolution value
;       kernel - kernel which we subtract from signal; we pad it in the program so no need for
;               sam dimensions as signal
; Return
;       Struct containing
;           struct.loc - coordinate at which we found signal
;           struct.amp - amplitude of signal subtracted
;           struct.sig - new signal (real part) after subtraction
;               * - if no signal found, amp = -1, loc = [-1,-1]
;
; convolves signal and kernel, finds the point with highest value of filter,
; computes amplitude by optimal filtering formula and subtracts out that value times
; kernel


    ; pad Kernel
    sizeSig = LONG(sqrt(N_ELEMENTS(signal)))
    sizeKer = LONG(sqrt(N_ELEMENTS(kernel)))
    paddedKernel = dblarr(sizeSig, sizeSig)
    paddedKernel[0,0] = kernel / max(kernel) ; should be normalized kernel

    ; ; first things first, use optimal filtered kernel for everything
    ; ; compute by padding to specdens size, divide, crop back out
    ; kernel0pad = dblarr(sizeSig, sizeSig)
    ; kernel0pad[0,0] = kernel 
    ; kkernel0 = fft_shift(fft(kernel0pad, /INVERSE))
    ; kkernel0 /= specDens ; optimal kernel
    ; kernelDiv = fft(fft_shift(kkernel0, /REVERSE))
    ; kernelDiv = kernelDiv[0:sizeKer - 1, 0:sizeKer - 1] / max(kernelDiv)

    ; ; pad Kernel
    ; paddedKernel = dblarr(sizeSig, sizeSig)
    ; normKer = max(kernel)
    ; paddedKernel[0,0] = kernel / normKer ; should be normalized kernel

    ; ; get k-space forms of stuff
    ; kkernel = fft_shift(fft(paddedKernel, /INVERSE))
    ; kSignal = fft_shift(fft(signal, /INVERSE))

    ; ;convolve and figure out position
    ; convSig = convol(signal, kernelDiv)

    ; get k-space forms of stuff
    kkernel = fft_shift(fft(paddedKernel, /INVERSE))
    kSignal = fft_shift(fft(signal, /INVERSE))

    ; compute optimal filter
    sizeSigMod = (sizeSig - 1) / 2
    sizeKerMod = (sizeKer - 1) / 2 ; useful for computing indicies of trimmed specDens
    specDensSmall = specDens[ sizeSigMod - sizeKerMod: sizeSigMod + sizeKerMod, $
        sizeSigMod - sizeKerMod + 1: sizeSigMod + sizeKerMod + 1]
    kkernel2 = fft_shift(fft(kernel, /INVERSE))
    kkernel2 /= specDensSmall
    kernel2 = fft(fft_shift(kkernel2, /REVERSE))
    kernel2 /= max(kernel2)

    ; convolve and find max
    convSig = convol(signal, kernel2)
    temp = max(convSig, maxPos); gives 2D position, but in single index.
        ; we convert
    temp = to2d(sizeSig, maxPos)
    maxX = temp[0]
    maxY = temp[1]
    maxX -= sizeKer / 2; IDL makes kernel centered, so this is simply
        ; correction for the amount the kernel was moved
    maxY -= sizeKer / 2

    ; we note in the formula we want a form e^(2 * pi * i * vec_k * vec_(x_0)). 
    kx = (replicate(1L, sizeSig) ## findgen(sizeSig)) - (sizeSig - 1) / 2
    ky = (findgen(sizeSig) ## replicate(1L, sizeSig)) - (sizeSig - 1) / 2

    ; key formula
    amp = real_part(TOTAL(exp(-2 * !PI * complex(0,1) * ((kx * maxX) + (ky * maxY)) / sizeSig) * $
        conj(kkernel) * kSignal / specDens) / TOTAL( conj(kkernel) * kkernel / $
        specDens))


    ; noise sigma, for detection threshold
    sigm = real_part(sqrt(1 / ( sizeSig^2 * TOTAL(kkernel * CONJ(kkernel) / specDens))))

    ;subtraction if passes threshold
    if amp ge 5 * sigm then begin
        newKernel = dblarr(sizeSig, sizeSig)
        newKernel[maxX, maxY] = amp * kernel
        return, {loc:[maxX, maxY], amp:amp, sig:(signal - newKernel), sigm:sigm}
    endif else begin
        return, {subtracted, loc:[-1,-1], amp:-1, sig:signal, sigm:sigm}
    endelse
end

; b=dblarr(50,50)
; a=gaussian_function([3,3])
; b[12,17] = a
