pro mapquadtest
range = 256
noiseHeight = 0.001
fitRange = 3 ; fit an area of +- fitRane around max

; generate random peak
gaussHeight = 1
sigma = 3
xcent = RANDOMU(SEED) * (range - 10 * sigma) + 5 * sigma
ycent = RANDOMU(SEED) * (range - 10 * sigma) + 5 * sigma




; generate Gaussian map
map = RANDOMN(SEED, range, range) * noiseHeight

; compute Gaussian signal only near center
for i = FIX(xcent - 5 * sigma), FIX(xcent + 5 * sigma - 1) do begin
    for j = FIX(ycent - 5 * sigma), FIX(ycent + 5 * sigma - 1) do begin
        map[i,j] += gaussHeight * exp(-((i - xcent)^2 + (j - ycent)^2) / $
            (2 * sigma^2))
    endfor
endfor




; fit map
temp = max(map, loc)
loc = to2d(range, loc)
xmax = loc[0]
ymax = loc[1]
submap = map[xmax - fitRange : xmax + fitRange, ymax - fitRange : ymax + fitRange]

params = fitquad(alog(submap))
xparam = params[2] / (2 * params[1])
yparam = params[3] / (2 * params[1])

print, [xparam + xmax - fitRange, yParam + ymax - fitRange]
print, [xcent, ycent]
print, 'surface, genquad(5, params[0], params[1],params[2],params[3])'
print, 'surface, alog(submap)'
stop
end
