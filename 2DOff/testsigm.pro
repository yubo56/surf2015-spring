pro testsigm, numTests
; tests uncertainty on sigm with numTests guesses at x0, y0
; explanation of magic number 3*sigm: addGauss only adds up to 3 sigma, so
;   we can truncate our kernel to width 3 sigma

; specdens params
power = 1
const = 0.01D ; units of PF (power flux)
range = 256L ; units of arccminutes
; gaussian parameters
sigm = 3D
Amp = 100 * const   ; height of Gaussian; now simeq 50 * noise height (noise height is really tall!)
fitRange = 3        ; fit an area of +- fitRange around max
; store deviations
xdevs = []
ydevs = []
Adevs = []

; set up some stuff to compute Gaussians
rand = RANDOMU(seed, 2 * numTests)
x0 = rand[0 : numTests - 1] * (range - 2 * fitRange - 1) + fitRange
y0 = rand[numTests : 2 * numTests - 1] * (range - 2 * fitRange - 1) + fitRange
sigm = replicate(sigm, numTests)
A = replicate(Amp, numTests)

; make specdens
k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1
specDens = make_array(range, range, VALUE=const) ; + 0.1 / k ^ power
    ; units of power flux (PF)^2
    ; use white noise for now
specDens *= 1.0 / range^2 ; units of PF^2 / arcmin^2

; go through tests
for i=0, numtests - 1 do begin
    ; make signal
    noise = genNoise(specDens) ; different realizations of noise each time
        ; units of PF
    signal = addgauss(a[i], sigm[i], x0[i], y0[i], noise) ; add gaussians
        ; still units of PF
    stop

    ; convolve map
    kSignal = fft_shift(fft(signal, /INVERSE)) / range
        ; manually insert units: PF / arcmin
    paddedKernel = addgauss(1, sigm[i], 0, 0, dblarr(range, range)) ; kernel is normalized
        ; no units
    kkernel = fft_shift(fft(paddedKernel, /INVERSE)) / range
        ; manually insert units; / arcmin
    ; convolve and figure out position
    convSig = fft(fft_shift(conj(kkernel),/REVERSE) * fft_shift(kSignal, /REVERSE) / specDens)
         ; optimal filter = kernel/specDens

    ; fit map
    temp = max(convSig, loc)
    loc = to2d(range, loc)
    xmax = loc[0]
    ymax = loc[1]

    ; fit peak on convolved map
    minx = max([xmax - fitRange, 0]) ; bounds checking
    maxx = min([xmax + fitRange, range - 1])
    miny = max([ymax - fitRange, 0])
    maxy = min([ymax + fitRange, range - 1])
    submap = convSig[minx:maxx, miny:maxy]
    params = fitquad(alog(submap))

    ; extract centers; backwards from expected because IDL x,y axis are flipped
    xparam = params[3] / (2 * params[1]) + xmax - fitRange
    xparam = xparam mod 256
        ; last term compensates for non-centered kernel
    yparam = params[2] / (2 * params[1]) + ymax - fitRange
    yparam = yparam mod 256

    ; build correct filter
    filter = addgauss(1, sigm[i], xparam, yparam, dblarr(range, range)) ; start with gaussian in correct place
    kFilter = fft_shift(fft(filter, /INVERSE)) / range
        ; again manually insert units; / arcmin 
    Aest = TOTAL(conj(kFilter) * kSignal / specDens) / TOTAL(conj(kFilter) * kFilter / specDens)

    ; print parameters
    ; print, 'Xparam: ' + string(xparam) + ' || X0' + string(x0[i])
    ; print, 'Yparam: ' + string(yparam) + ' || Y0' + string(y0[i])
    ; print, 'A     : ' + string(Aest) + ' || A0' + string(A[i])
    ; print, 'sigma : ' + string(sigmEst) + ' || A0' + string(sigm[i])

    ; sanity checks
    if abs(x0[i] - xparam) ge 1 then stop
    if abs(y0[i] - yparam) ge 1 then stop
    if imaginary(Aest) ge real_part(Aest) / 10 then stop

    xdevs = [xdevs, x0[i] - xparam]
    ydevs = [ydevs, y0[i] - yparam]
    Adevs = [Adevs, A[i] - real_part(Aest)]
endfor

; compute expected sigmas
sigm_x0 = SQRT( REAL_PART(range^2 / (TOTAL( (fft_shift(dist(range, 1)^2) # replicate(1.0 / range^2, range)) * conj(kfilter) * kfilter / specDens )))) / (2 * !PI * Amp)
sigm_x0 = SQRT( REAL_PART(range^2 / (TOTAL( (replicate(1.0 / range^2, range) # fft_shift(dist(range, 1)^2)) * conj(kfilter) * kfilter / specDens )))) / (2 * !PI * Amp)
    ; replicate contains a 1/range^2 b/c nu_x needs units of 1/range, 1/arcmin
sigm_A = REAL_PART(SQRT(range^2 / ( TOTAL(kfilter * CONJ(kfilter) / specDens))))
    ; don't forget bin width in integral

; generate histograms
hist = histogram(xdevs, binsize=sigm_t0 / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_t0
plot, bins, hist
oplot, bins, gfit
write_png, 'xdevs.png', tvrd()
plot, bins, alog(hist)
write_png, 'xdevsLog.png', tvrd()

hist = histogram(ydevs, binsize=sigm_t0 / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_t0
plot, bins, hist
oplot, bins, gfit
write_png, 'ydevs.png', tvrd()
plot, bins, alog(hist)
write_png, 'ydevsLog.png', tvrd()

hist = histogram(Adevs, binsize=sigm_A / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_A
plot, bins, hist
oplot, bins, gfit
write_png, 'amps.png', tvrd()
plot, bins, alog(hist)
write_png, 'ampsLog.png', tvrd()

end
