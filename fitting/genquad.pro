function genquad, range, c1, c2, c3, c4
; returns a quadratic with linearised C_1, C_2, C_3, C_4 parameters
; f(x) = c1 - c2 * (x^2 + y^2) + c3 * x + c4 * y



    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ACTUAL CODE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; store the return array
    arr = make_array(range, range, VALUE = double(c1))

    ; compute x,y components
    xarr = dindgen(range) ## replicate(1D, range)
    yarr = replicate(1D, range) ## dindgen(range)

    ; execute arr = height - xarr^2 - yarr^2
    arr -= c2 * (xarr^2 + yarr^2)
    arr += c3 * xarr
    arr += c4 * yarr

    ; return
    return, arr
end
