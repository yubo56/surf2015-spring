pro quadfunc, XY, A, F, pder
; fitting function for quadratic with zero cutoff
; xy is n * 2 array of possible values for x,y to take
; A are parameters
; F is the computed n*n for each of the x,y values
; pder (optional) is partial derivatives
    print, A
    X = double(XY[*,0])
    Y = double(XY[*,1])
    A = double(A)
    range = long(n_elements(Y))
    
    ; compute x, y compomponents of F
    xf = (A[1] * X^2 - A[2] * X)
    yf = (A[1] * Y^2 - A[3] * Y)
    
    ; add
    ; F = A0 - A1*(X^2 + Y^2) + A2*X + A3*Y
    F = make_array(range, range, value=double(A[0]))
    F -= xf ## replicate(1D, range)
    F -= replicate(1D, range) ## yf
    F[ WHERE(F LT 0, /NULL) ] = 0
    F = reform(F, range^2)

    ; get pders
    ; pd(A0) = 1
    ; pd(A1) = -(X^2 + Y^2)
    ; pd(A2) = X
    ; pd(A3) = Y
    pder = [[make_array(range^2,value=1D)], $
        [reform(-X^2 ## replicate(1D,range) - replicate(1D,range) ## Y^2, range^2)], $
        [reform(X ## replicate(1D,range), range^2)], [reform(replicate(1D,range) ## Y, range^2)]]
end

; so if we want Gaussian centered (100,100), height 100, sigma 10
; C1 = -9900
; C2 = 1/2
; C3 = 100
; C4 = 100


; b = indgen(256)
; a = [-9900,0.5,100,100]
; test=genquad(256,130,145,100,5)
; quadfunc, [[b],[b]], a, out, pder
; fit = curvefit([[b],[b]], reform(test,256L^2,1), qqq, a, FUNCTION_NAME='quadfunc')
