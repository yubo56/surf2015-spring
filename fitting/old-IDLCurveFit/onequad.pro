pro onequad, X, A, F, pder
; fitting function for 1D quadratic with zero cutoff
    print, A
    X = double(X)
    A = double(A)
    range = long(n_elements(X))
    
    ; add
    ; F = A0 - A1*X^2 + A2*X
    F = make_array(range, value=A[0])
    F -= (A[1] * X^2 - A[2] * X)
    F[ WHERE(F LT 0, /NULL) ] = 0

    ; get pders
    ; pd(A0) = 1
    ; pd(A1) = -X^2
    ; pd(A2) = X
    pder = [[make_array(range,value=1D)], [-X^2], [X]]
end

; so if we want Gaussian centered (100,100), height 100, sigma 10
; C1 = -9900
; C2 = 1/2
; C3 = 100
; C4 = 100


; b = indgen(256)
; a = [-9900,0.5,100,100]
; test=genquad(256,130,145,100,5)
; quadfunc, [[b],[b]], a, out, pder
; fit = curvefit([[b],[b]], reform(test,256L^2,1), qqq, a, FUNCTION_NAME='quadfunc')
