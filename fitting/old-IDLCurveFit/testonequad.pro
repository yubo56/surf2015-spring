pro testonequad
; tests curvefit on onequad
X = indgen(100)
onequad, X, [-24000, 10, 1000], quadratic
params = [-24000, 9, 1000]
fit = curvefit(X, quadratic, qqq, params, function_name='onequad', /NODERIVATIVE)
stop
end

; derivative in onequad isn't computed correctly
