pro testquadfunc
; tests curvefit on quadfunc
X = indgen(256)
Y = indgen(256)
quadfunc, [[X],[Y]], [-9900, 0.5, 100, 100], quadratic
gaussian = dblarr(256,256)
gaussian[69,69] = gaussian_function([10,10])
gaussian = reform(gaussian, 256L^2,1)

params = [-9950, 0.5, 100, 100]
fit = curvefit([[X],[Y]], gaussian, qqq, params, function_name='quadfunc', /NODERIVATIVE)
stop
end

; derivative doesn't work!
; A=100, sigma = 10
