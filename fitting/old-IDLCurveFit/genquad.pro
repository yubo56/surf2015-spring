function genquad, range, xcent, ycent, height, width
    ; return an array of size range x range with upside-down quadratic centered at
    ; xcent, ycent. Returned array is truncated such that no negative values
    ; places quadratic
    ; z(x,y) = height - ((x - x0)^2 - (y - y0)^2) / width

    ; NOTE: xcent, ycent assume array is indexed [0:range, 0:range] not 
    ;   [-range/2:range/2] etc.



    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ACTUAL CODE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; store the return array
    arr = dblarr(range, range)

    ; compute x,y components
    xarr = dindgen(range) ## replicate(1L, range)
    xarr -= xcent ; x - x0
    yarr = replicate(1L, range) ## dindgen(range)
    yarr -= ycent ; y - y0

    ; execute arr = height - xarr^2 - yarr^2
    arr += height
    arr -= (xarr^2 / width)
    arr -= (yarr^2 / width)

    ; zero negative elements
    arr[ WHERE(arr LT 0, /NULL) ] = 0
    return, arr
end
