pro fitquadtest
range = 9
; z = genquad(range, 5, 0.1, 1, 1)
; x0 = 5, y0 = 5, A = 1, sigma = sqrt(5)
; expect C1 = 5, C2 = 1/10, C3 = 1, C4 = 1
gaussian = GAUSSIAN_FUNCTION([sqrt(5), sqrt(5)])
z = alog(gaussian[3:11, 3:11])

params = fitquad(z)
fit = genquad(range, params[0], params[1], params[2], params[3])
print, params
print, 'surface, EXP(fit)'
stop
print, 'surface, gaussian[3:11, 3:11]'
end
