function subtractmax, signal, kernel, specDens
; Arguments
;       signal - real signal from which to subtract kernel once, at max convolution value
;       kernel - kernel which we subtract from signal; we pad it in the program so no need for
;               sam dimensions as signal
; Return
;       Struct containing
;           struct.loc - coordinate at which we found signal
;           struct.amp - amplitude of signal subtracted
;           struct.sig - new signal (real part) after subtraction
;               * - if no signal found, amp = -1, loc = [-1,-1]
;
; convolves signal and kernel, finds the point with highest value of filter,
; computes amplitude by optimal filtering formula and subtracts out that value times
; kernel
    ; pad Kernel
    sizeSig = LONG(sqrt(N_ELEMENTS(signal)))
    paddedKernel = dblarr(sizeSig, sizeSig)
    paddedKernel[0,0] = kernel / max(kernel) ; should be normalized kernel

    ; to compute optimal filter, just transpose of normalized kernel
    kkernel = fft_shift(fft(paddedKernel, /INVERSE))
    kkernel0 = conj(kkernel / specDens) ; optimal kernel
    kernelDiv = fft(fft_shift(kkernel0, /REVERSE))

    ; convolve and figure out position
    convSig = fft(fft(kernelDiv) * fft(signal), /INVERSE)
    ; IDLs is so slow, use own
    ; convSig = convol(signal, reverse(reverse(kernelDiv), 2), center=0, /edge_wrap)

    maxval = max(convSig, maxPos) ; gives 2D position, but in single index.
        ; 'temp = ' here is just to surpress output
    temp = to2d(sizeSig, maxPos) ; convert to positions
    maxX = temp[0]
    maxY = temp[1]

    ; get k-space forms of stuff
    kSignal = fft_shift(fft(signal, /INVERSE))

    ; we note in the formula we want a form e^(2 * pi * i * vec_k * vec_(x_0)). 
    kx = (replicate(1L, sizeSig) ## findgen(sizeSig)) - (sizeSig - 1) / 2
    ky = (findgen(sizeSig) ## replicate(1L, sizeSig)) - (sizeSig - 1) / 2

    ; key formula
    amp = real_part(TOTAL(exp(-2 * !PI * complex(0,1) * ((kx * maxX) + (ky * maxY)) / sizeSig) * $
        conj(kkernel) * kSignal / specDens) / TOTAL( conj(kkernel) * kkernel / $
        specDens))


    ; noise sigma, for detection threshold
    sigm = real_part(sqrt(1 / ( sizeSig^2 * TOTAL(kkernel * CONJ(kkernel) / specDens))))

    ;subtraction if passes threshold
    if amp ge 5 * sigm then begin
        newKernel = dblarr(sizeSig, sizeSig)
        newKernel[maxX, maxY] = amp * kernel
        return, {loc:[maxX, maxY], amp:amp, sig:(signal - newKernel), sigm:sigm}
    endif else begin
        return, {subtracten, loc:[-1,-1], amp:-1, sig:signal, sigm:sigm}
    endelse
end
