pro filterNoise, numSamples, gaussHeight, numGaussians
; arguments 
;       numSamples - Number of times to try  subtracting some number of pulses
;       gaussHeight - height of Gaussians being inserted (width/number are fixed in program)
;       numGaussians - number of Gaussians to insert and seek



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 256.                                        ; size of grid
downRange = 128                                     ; size of downsampled grid
power = 2                                           ; constants of the white noise
const = 0.01
gaussPixWidth = 5                                   ; pt source width in number of pixels

; gaussian kernel
gaussian = GAUSSIAN_FUNCTION ( [( gaussPixWidth - 1 ) / 2, ( gaussPixWidth - 1 ) / 2])

    


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; store cumulative amplitudes, moments
ampsCum = []
momentsCum = []

; make specdens
k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1
specDens = 0.1 / k ^ power + const


; test this for some number of iterations
for trial=1, numSamples do begin
    ; make noise in k space, random every time
    noise = genNoise(specDens)

    ; add Gaussian of height
    retVal = addfunc(noise, gaussHeight * gaussian, numGaussians)
    signal = retVal.sig
    xcoords = retVal.xs
    ycoords = retVal.ys

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;OPTIMAL FILTERING;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Code works by first finding pulses, then constructing matrix and inverting

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; FIND PULSES;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    orig = signal ; keep a copy for matrix computation later
    foundX = [] ; store found locations
    foundY = []

    ; let's just subtract numSamples for now until we know our matrix implementation works
    for i=1, numGaussians do begin
        retVal = subtractmax(signal, gaussian, specDens)
        loc = retVal.loc
        signal = retVal.sig

        ; ; add to foundX, foundY iff not already in, else break;
        ; ;       keeping for future reference but don't need here since we subtract exactly numGaussians
        ;  ind = where(abs(foundY - loc[1]) le 2)

        ; if (ind ne -1) and (where(abs(foundY - loc[1]) le 2) eq ind) then begin
        ;     ; found already, so break
        ;     break
        ; endif else begin; not already found, so add
            foundX = [foundX, loc[0]]
            foundY = [foundY, loc[1]]
        ; endelse
    endfor

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; matrix solve the problem ;;;;;;;;;;;;;;;;;;;;;;;

    retVal = subtractmat(orig, gaussian, specDens, foundX, foundY)
    ampsCum = [[ampsCum], [retVal.amps]]
    momentsCum = [[momentsCum], [moment(retVal.amps)]]
endfor


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;           UNCOMMENT FOR TESTSIG                    ;;;;;;;;;;;;;;
; plot, retVal.amps, /ynozero
; oplot, replicate(gaussHeight + retVal.sigm, n_elements(retVal.amps))
; oplot, replicate(gaussHeight - retVal.sigm, n_elements(retVal.amps))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;           UNCOMMENT FOR TESTMULT                   ;;;;;;;;;;;;;;
if numSamples ne 1 then begin
    print, TOTAL(moments2Cum, 2)/numSamples ; prints sigma
endif else begin
    print, moments2Cum
endelse
print, [ retVal.sigm, retVal.sigm^2 ]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
stop
END
