function subtractmat, signal, kernel, specdens, foundX, foundY
; Arguments
;       signal - real signal from which to subtract kernel once, at max convolution value
;       kernel - kernel which we subtract from signal; we pad it in the program so no need for
;               sam dimensions as signal
;       specdens - spectral density of noise
;       foundX - x-locations of pulses to examine
;       foundY - y-locations of pulses to examine

; Return
;       Struct containing
;           struct.amps - amplitudes of pulses
;           struct.sigm - theoretical sigma of pulses
;
; convolves signal and kernel, finds the point with highest value of filter,
; computes amplitude by optimal filtering formula and subtracts out that value times
; kernel

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; CONSTANTS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

j = -complex(0,1)                                   ; negative since we have fourier convention backwards 
                                                    ;(is correct normalization coefficient this way)


; get k-space forms of stuff
sizeSig = LONG(sqrt(N_ELEMENTS(signal)))
sizeKer = LONG(sqrt(N_ELEMENTS(kernel)))
    ; pad Kernel
paddedKernel = dblarr(sizeSig, sizeSig)
paddedKernel[0,0] = kernel / max(kernel) ; should be normalized kernel
kkernel = fft_shift(fft(paddedKernel, /INVERSE))
kSignal = fft_shift(fft(signal, /INVERSE))

; we note that in the matrix formula rquire dotting against a 2 * pi * nu
kx = 2 * !PI * j * ((replicate(1L, sizeSig) ## findgen(sizeSig)) - (sizeSig - 1) / 2)
ky = 2 * !PI * j * ((findgen(sizeSig) ## replicate(1L, sizeSig)) - (sizeSig - 1) / 2)
; kx * x + ky * y = j * \vec{k} \cdot \vec{x}_0

    ; we next need an array with M_{ik} = \vec{x}_i - \vec{x}_k, the vectors of each point
numPeaks = n_elements(foundX) ; we could just use numSamples
    ; compute horizontal vector
vecx = (replicate(1D, numPeaks) ## foundX) 
vecy = (replicate(1D, numPeaks) ## foundY)
Mx = vecx - transpose(vecx)
My = vecy - transpose(vecy)

; vector operation wizardry; see documentaton
Mx = reform(Mx, numPeaks^2) ## reform(kx, sizeSig^2) / sizeSig
My = reform(My, numPeaks^2) ## reform(ky, sizeSig^2) / sizeSig
integrandM = replicate(1D, numPeaks^2) ## reform(conj(kkernel) * kkernel / specDens, sizeSig^2)
integrandS = replicate(1D, numPeaks) ## reform(conj(kkernel) * kSignal / specDens, sizeSig^2)
M = TOTAL(TOTAL(reform(exp(Mx + My) * integrandM, [sizeSig, sizeSig, numPeaks, numPeaks]), 1), 1)
Sx = foundX ## reform(kx, sizeSig^2) / sizeSig
Sy = foundY ## reform(ky, sizeSig^2) / sizeSig
S = TOTAL(TOTAL(reform(exp(Sx + Sy) * integrandS, [sizeSig, sizeSig, numPeaks]), 1), 1)

; calculate amps!
amps = invert(M) # S
if max(imaginary(amps)) ge 1e-6 then stop ; if too big imaginary part (should be zero!) stop

; get sigma for return
sigm = real_part(sqrt(1 / ( sizeSig^2 * TOTAL(kkernel * CONJ(kkernel) / specDens))))

return, {amps:real_part(amps), sigm: sigm}

END
