pro testinterp, numSamples
; arguments 
;       numSamples - Number of times to try  subtracting some number of pulses
;       gaussHeight - height of Gaussians being inserted (width/number are fixed in program)
;       numGaussians - number of Gaussians to insert and seek



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 256.                                        ; size of grid
power = 2                                           ; constants of the white noise
const = 0.01
gaussPixWidth = 5                                   ; pt source width in number of pixels
rebinFactor = 2
gaussHeight = 0.005
numGaussians = 20

; gaussian kernel
gaussian = GAUSSIAN_FUNCTION ( [( gaussPixWidth - 1 ) / 2, ( gaussPixWidth - 1 ) / 2])
    


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; make specdens
k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1
specDens = 0.1 / k ^ power + const

rebinDat = dblarr(numSamples)
cubicDat = dblarr(numSamples)
cubic0Dat = dblarr(numSamples)
cubic1Dat = dblarr(numSamples)

; test this for some number of iterations
for trial=0, numSamples - 1 do begin
    ; make noise in k space, random every time
    noise = genNoise(specDens)

    ; add Gaussian of slightly random height
    retVal  = addfunc(noise, gaussHeight * gaussian, numGaussians)
    orig    = retVal.sig
    xcoords = retVal.xs
    ycoords = retVal.ys

    ; test interpolations
    signal = rebin(orig, range/rebinFactor, range/rebinFactor)
    orig /= sqrt(TOTAL(orig^2))

    diff = rebin(signal, range, range)
    diff /= sqrt(total(diff^2))
    rebinDat[trial] = total(convol(orig, diff))

    diff = interpolate(signal, findgen(range)*(range/rebinFactor)/(range - 1), findgen(range)*(range/rebinFactor)/(range - 1), /GRID, CUBIC=0)
    diff /= sqrt(total(diff^2))
    cubic0Dat[trial] = total(convol(orig, diff))

    diff = interpolate(signal, findgen(range)*(range/rebinFactor)/(range - 1), findgen(range)*(range/rebinFactor)/(range - 1), /GRID, CUBIC=-0.41)
    diff /= sqrt(total(diff^2))
    cubicDat[trial] = total(convol(orig, diff))

    diff = interpolate(signal, findgen(range)*(range/rebinFactor)/(range - 1), findgen(range)*(range/rebinFactor)/(range - 1), /GRID, CUBIC=-1)
    diff /= sqrt(total(diff^2))
    cubic1Dat[trial] = total(convol(orig, diff))
endfor

print, 'rebin     ' + '     cubic0Dat     ' + '     cubicDat     ' + '     cubic1Dat'
print, transpose([[rebinDat], [cubic0Dat], [cubicDat], [cubic1Dat]])
stop
end
