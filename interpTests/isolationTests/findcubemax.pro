pro findcubemax
; No parameters accepted
; simply uses brute force to find minimum CUBIC parameter to cubic interpolate
;   to interpolate a Gaussian

rebinNum = 3                                    ; number of bins to rebin
orig = gaussian_function([10,10])               ; original Gaussian, to reconstruct
orig /= sqrt(total(orig^2))
c = rebin(orig, rebinNum, rebinNum)             ; rebinned, to reconstruct from this
numDivs = 100.0                                 ; number of divisions
convols = dblarr(numDivs + 1)                   ; RMS values for each parameter value

for i=0,numDivs do begin
    diff = interpolate(c, findgen(63)*rebinNum/62, findgen(63)*rebinNum/62, /GRID, CUBIC=-i/numDivs)
    diff /= sqrt(TOTAL(diff^2))
    convols[i] = TOTAL(convol(orig, diff))
endfor

plot, findgen(numDivs + 1)/numDivs, convols, /ynozero
write_png, strcompress('findcubemax' + string(rebinNum) + '.png', /remove_all), tvrd()
temp = max(convols[1:*],loc)
print, loc / 100.0
END

; results @ 21: 0 beats all, 0.37 is next best
; results @ 9: 0 beats all, 0.35 is next best
; results @ 3: 0.99 beats 0 which beats most
