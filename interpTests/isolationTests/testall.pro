pro testall
; No parameters accepted
; simply uses brute force to find minimum CUBIC parameter to cubic interpolate
;   to interpolate a Gaussian

binParam = 3                                        ; number of bins in initial rebin
orig = gaussian_function([10,10])                   ; original Gaussian, to reconstruct
orig /= sqrt(total(orig^2))
c = rebin(orig, binParam, binParam)                 ; rebinned, to reconstruct from this

;orig
; surface, orig
; write_png, 'orig.png', tvrd()

;rebin
diff = rebin(c, 63, 63)
diff /= sqrt(total(diff^2))
; surface, diff
; write_png, 'rebin.png', tvrd()
; surface, orig - diff
; write_png, 'rebinDiff.png', tvrd()
print, 'rebin '  + strtrim(string(total(convol(orig, diff))))

;cubic = 0
diff = interpolate(c, findgen(63)*binParam/62, findgen(63)*binParam/62, /GRID, CUBIC=0)
diff /= sqrt(total(diff^2))
; surface, diff
; write_png, 'cubic0.png', tvrd()
; surface, orig - diff
; write_png, 'cubic0Diff.png', tvrd()
print, 'cubic0 ' + strtrim(string(total(convol(orig, diff))))

; cubic = -0.35
; diff = interpolate(c, findgen(63)*binParam/62, findgen(63)*binParam/62, /GRID, CUBIC=-0.35)
; diff /= sqrt(total(diff^2))
; surface, diff
; write_png, 'cubic31.png', tvrd()
; surface, orig - diff
; write_png, 'cubic31Diff.png', tvrd()
; print, 'cubic1 ' + strtrim(string(total(convol(orig, diff))))

; min_curve_surf
diff = min_curve_surf(c, nx=63, ny=63)
diff /= sqrt(total(diff^2))
; surface, diff
; write_png, 'min_curve_surf.png', tvrd()
; surface, orig - diff
; write_png, 'min_curve_surf_Diff.png', tvrd()
print, 'mincurvesurf ' + strtrim(string(total(convol(orig, diff))))

; tri_surf
diff = tri_surf(c, nx=63, ny=63)
diff /= sqrt(total(diff^2))
; surface, diff
; write_png, 'tri_surf.png', tvrd()
; surface, orig - diff
; write_png, 'tri_surf_Diff.png', tvrd()
print, 'trisurf ' + strtrim(string(total(convol(orig, diff))))

END

; interpolate from 21
; rebin             0.994998
; cubic0            0.989099
; mincurvesurf      0.999349
; trisurf           0.999346

; interpolate from 9
; rebin             0.956633
; cubic0            0.943753
; mincurvesurf      0.992128
; trisurf           0.992526

; interpolate from 3
; rebin             0.659485
; cubic0            0.641989
; mincurvesurf      0.843711
; trisurf           0.831252

