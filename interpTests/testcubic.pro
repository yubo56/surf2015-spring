pro testcubic
; tests cubic interp for our Gaussians



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;; SIGNAL PARAMETERS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 256.                                        ; size of grid
power = 2                                           ; constants of the white noise
const = 0.01
gaussPixWidth = 5                                   ; pt source width in number of pixels
rebinFactor = 2
gaussHeight = 0.0005
numGaussians=10
gaussian = GAUSSIAN_FUNCTION ( [( gaussPixWidth - 1 ) / 2, ( gaussPixWidth - 1 ) / 2])

; binning parameters
rebinNum = range / 2                                    ; number of bins to rebin
numDivs = 100.0                                 ; number of divisions

; make some noise!
k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1
specDens = 0.1 / k ^ power + const
noise = genNoise(specDens)
retVal  = addfunc(noise, gaussHeight * gaussian, numGaussians)
orig    = retVal.sig
orig /= sqrt(total(orig^2))

; compute rebinned, also store things
c       = rebin(orig, rebinNum, rebinNum)
convols = dblarr(numDivs + 1)                   ; RMS values for each parameter value

for i=0,numDivs do begin
    diff = interpolate(c, findgen(range)*rebinNum/(range - 1), findgen(range)*rebinNum/(range - 1), /GRID, CUBIC=-i/numDivs)
    diff /= sqrt(TOTAL(diff^2))
    convols[i] = TOTAL(convol(orig, diff))
endfor
plot, findgen(numDivs + 1)/numDivs, convols, /ynozero
temp = max(convols[1:*],loc)
print, loc / 100.0
end
