PRO oldFilterNoise

;things checked:
; Fourier transforms in IDL are the same as us
; fixed a factor of Sqrt[2T] in sigma
; TODO - random +6 in paddedGauss (fixes stuff though)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

range = 512 ; range, the full range will go from [0, N] U [-N, -1]
numSamples = 100 ; number of samples to take to compute estimator of A
)
;noise params
b = 1 ; power of the power law for noise
C = 0.01 ; overall constant profile for noise

;filter params
filterPower = 1 ; we use an n-th power filter
tau = 10. / range ; characteristic decay length of high-pass

;pt source params
gaussWidth = 7; width in number of pixels
gaussHeight = 0.5; characteristic height of noise
numGaussians = 1 ; number of Gaussians to insert





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GENERATE NOISE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; make spectral power density in k space
k = FINDGEN(range + 1) + 1
T = 2 * range + 1
specDens = 1 / k ^ b + C
specDensFull = [ specDens, REVERSE( specDens[ 1 : *] ) ]
sigma = sqrt(specDens) / sqrt(4 * T)

ahats = [];

; generate Gaussian noise in k space; we require f(k) = f(-k)*
for sample = 0, (numSamples - 1) do begin
    rePt1 = sigma * RANDOMN(SEED, range + 1); [0, N]
    imPt1 = sigma * RANDOMN(SEED, range + 1)
    rePt = [ rePt1, REVERSE( rePt1[ 1 : *] ) ]; extend to [0, N] U [-N, -1]
    imPt = [ imPt1, -REVERSE( imPt1[ 1 : *] ) ]; ensure f_k = f_(-k)^*
    knose = COMPLEX( rePt, imPt ); total knoise, sorry for weird name

    ; perform the FFT
    noise = REAL_PART(FFT(knose))
    ; print, max(imaginary(noise)); check noise is fully real
    ; PLOT, noise; plot the results

    ; insert Gaussians
    gaussian = gaussHeight * GAUSSIAN_FUNCTION ( ( gaussWidth - 1 ) / 2)
            ; IDL generates Gaussians of FWHM (width-1)/2
    ; startIndex = FIX( ( N_ELEMENTS(noise)) * RANDOMU(Seed, numGaussians))
            ; generates numGaussian integers between [0, noise.len]
    startIndex = (N_ELEMENTS(noise) - N_ELEMENTS(gaussian)) / 2
            ; centers Gaussian on [0] element, keeping in mind IDL's stupid convention
            ; with FFT signals ([0, N] U [-N,-1] in order) is only in freq space; here
            ; we only want to center it halfway, including the width of the Gaussian
    for i = 0, numGaussians - 1 do begin ; insert each Gaussian
        for j = 0, N_ELEMENTS(gaussian) - 1 do begin ; insert each point of Gaussian
            noise[(startIndex[i] + j) MOD N_ELEMENTS(noise)] += gaussian[j] 
                    ; cyclic-ness of signal
        endfor
    endfor

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;OPTIMAL FILTERING;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; make necessary FFTs
    kSignal = FFT(noise, /INVERSE) 
        ; note that the noise variable actually carries the signal + noise
    paddedGauss = [MAKE_ARRAY((N_ELEMENTS(kSignal) - N_ELEMENTS(gaussian)) / 2), gaussian, $
        MAKE_ARRAY(N_ELEMENTS(kSignal) - (N_ELEMENTS(kSignal) + N_ELEMENTS(gaussian)) / 2)] / gaussHeight
        ; putting the Gaussian in the middle and padding with zeroes
        ; follow the same positioning as startIndex
    KGauss = FFT(paddedGauss , /INVERSE)

    ; compute Ahat
    ahat = TOTAL(CONJ(kGauss) * kSignal / specDensFull) / $
        TOTAL(kGauss * CONJ(kGauss) / specDensFull)

    ahats = [ahats, ahat]
    ; anum = abs(CONJ(kSignal) * kGauss / specDensFull)
    ; adenom = kGauss * CONJ(kGauss) / specDensFull

    ; print, TOTAL(anum)
    ; print, TOTAL(adenom)
ENDFOR

print, moment(ahats)
print, 1 / (2 * T * TOTAL(kGauss * CONJ(kGauss) / specDensFull))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;FILTER NOISE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; convolve with high-pass filter
; filter1 = ( COMPLEX(0, tau * k) / (COMPLEX (1, tau * k)) )^filterPower; filter
; filter = [ filter1, REVERSE( filter1[ 1 : *] ) ]; extend filter to all k space
; kNoise = FFT(noise, /INVERSE) ; inverse transform to get the kNoise
; kNoiseConv = kNoise * filter ; convolved

; ; convolve with Gaussians
; gaussian = [ gaussian, make_array(N_ELEMENTS(kNoise) - N_ELEMENTS(gaussian)) ]
;         ; pad with zeroes so right length under FFT
; kGaussian = FFT(gaussian, /INVERSE)
; kNoiseConv *= kGaussian

; ;reFFT back
; noise2 = FFT(kNoiseConv)
; PLOT, noise2
; ; print, startIndex

END
